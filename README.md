
# Remote Everything Matrix Multiplication Jobs

## Setup:
These instruction are designed for a local scheduler.
If the product scheduler is used, then the somebody with ADMIN permission needs to publish the dcp modules and enable the compute group support.

0) cd remoteEverything
1) Edit publish to change ROOT, DCP, DCP_CLIENT and DCP_SCHEDULER_LOCATION appropriately.
2) ./publish # publish the dcp-packages and npm install the other packages
3) ./joinCG  # join compute group joinKey=demo joinSecret=dcp; allows greedy infinite estimation.
4) Search for numGroups and change its value to be the max number of sandboxes ifor your worker.
5) git clone git@gitlab.com:Distributed-Compute-Protocol/dcp-rds.git # Install dcp-rds
6) cd dcp-rds
7) bin/dcp-rds-httpd # start up dcp-rds

# mxm:  C = A * B where A is m x k, B is k x n, C is m x n
Each matrix is a single Float32Array which is folded in the column direction (called a column major matrix.)
The matrix A is a workFn argument.
The slices are from a partition of B.
remoteJob, remoteColJob, remoteGroupedColJob all have remote slices, remote arguments, remote workFn, remote results.
remoteJob chops up the Float32Array underlying B into N sub-Float32Arrays where N = # of sandboxes.
remoteColJob chops up the Float32Array underlying B into n sub-Float32Arrays where n = # of columns of B.
remoteGroupedColJob chops up B's underlying Floiat32Array into n columns (just like remoteColJob) and then groups the columns into N arrays of colummns.
Thus remoteJob and remoteGroupedColJob have N slices, whereas remoteColJob has n slices.

## Running remoteJob
1) cd remoteJob
2) node dcp_mm_rds_upload.js # populate dcp-rds
3) node dcp_mm_remote.js     # Execute job

## Running remoteColJob
1) cd remoteColJob
2) node dcp_col_mm_rds_upload.js # populate dcp-rds
3) node dcp_col_mm_remote.js     # Execute job

## Running remoteGroupedColJob
1) cd remoteGroupedColJob
2) node dcp_gcol_mm_rds_upload.js # populate dcp-rds
3) node dcp_gcol_mm_remote.js     # Execute job

## NOTE: 
dcp-rds/remoteEverything/remote-helper.js makes setting up remote data job very easy.\
E.g.\
  const { RemoteHelper } = require('../remote-helper');\
  const rdsHelper = new RemoteHelper(rdsUrl, baseJobId/*, jobGraphId*/);\
  // Remote slices.\
  const remoteSlices = rdsHelper.slices(sliceCount, theJob);\
  // Remote workFn.\
  const remoteWorkFn = rdsHelper.workFn();\
  // Remote args.\
  const remoteMlData = rdsHelper.arguments([ 'array', 'stride' ]);\
  // Setup remote result storage on RDS.\
  rdsHelper.resultStorage(job); // const job = compute.for(....);\


