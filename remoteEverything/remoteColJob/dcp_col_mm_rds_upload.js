
//require('./cjs2-node');
const { Matrix32 } = require('matrix/matrix');
const { typedArrayToEncodedBase64 } = require('decode/decode');

const TEST = true;
const USE_FIXED_MXM = false;

const theJob = 77;
/** @type {number} */
let max1;
/** @type {number} */
let max2;
/** @type {Matrix32} */
let m1
/** @type {Matrix32} */
let m2;
if (USE_FIXED_MXM) {
  max1 = max2 = 2;
  ({ m1, m2 } = Matrix32.generateFixedMxMExample());
} else {
  max1 = max2 = 512;
  ({ m1, m2 } = Matrix32.generateMxMExample(max1, max2));
}

let start = Date.now();
const m3 = m1.mm(m2);
console.log(`Standalone MxM dt: ${m3.maxRow} x ${m3.maxCol}: ts ${Date.now() - start}ms`);
console.log('---------------------------------------------------------------');

function workFn(slice, array, stride) {
  //console.log('workFn1:', slice.length, array.length, stride);
  const { base64ToUint8Array } = require('decode');
  const { Matrix32 } = require('matrix');

  slice = base64ToUint8Array(slice);
  slice = new Float32Array(slice.buffer);

  array = base64ToUint8Array(array);
  array = new Float32Array(array.buffer);
  const matrix = new Matrix32(stride, array);
  //console.log('workFn2:', slice.length, array.length, stride);

  const _start = Date.now();
  progress();
  const result = matrix.mv(slice);
  progress();
  console.log('dt_done', Date.now() - _start);

  return result;
}

const XMLHttpRequest = require('xmlhttprequest-ssl');
let outCnt = 0;

function upload(str, element, elementType, contentType='text/plain'/*'application/octet-stream'*/, job=theJob)
{
  let url = 'http://127.0.0.1:3521/methods/upload';

  const xhr = new XMLHttpRequest();
  xhr.open("POST", url, true);
  xhr.onload = (event) => {
    if (outCnt++ < 15) console.log(xhr.status, xhr.responseText);
  };
  const body = `element=${element}&contentType=${contentType}&job=${job}&elementType=${elementType}&content=${str}`;
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhr.send(body);
}

// workFn
upload(workFn.toString(), 0, 'workFn');

// args
const b64 = typedArrayToEncodedBase64(m1.array);
if (TEST)
{
  const enc_b64 = decodeURIComponent(b64);
  const { base64ToUint8Array } = require('decode');
  const b8 = base64ToUint8Array(enc_b64);
  const ta = new Float32Array(b8.buffer);
  console.log('m1', m1.array.length, b64.length, ta.length);
}
console.log('Upload args', m1.array.length, b64.length); //, b64);
upload(b64, 'array', 'arg');
upload(m1.stride.toString(), 'stride', 'arg');

// slices
const m2_columns = m2.toColumns();

let _m2_column = m2_columns[0];
console.log('A column of Matrix32', _m2_column.length);
start = Date.now();
const _m3 = m1.mv(_m2_column);
console.log(`_m3 dt: ${_m3.length} x 1:`, Date.now() - start);
console.log('---------------------------------------------------------------');

upload(m2_columns.length.toString(), 0, 'slice');
for (let k = 0; k < m2_columns.length; k++) {
  const column = m2_columns[k];
  const slice = typedArrayToEncodedBase64(column);
  if (k < 15) console.log('Upload slice:', slice.length); //, slice);
  upload(slice, k+1, 'slice');
}
