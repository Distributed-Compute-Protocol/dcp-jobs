
// @ts-check
'use strict';

process.on('unhandledRejection', (e) => {
  console.log('....got a reject')
  console.error('****crapola', e)
});

const theJob = 77;

//const scheduler = 'https://scheduler.distributed.computer';
const scheduler = 'http://scheduler.paul.office.net';

var startTime = Date.now();
function timeSpan() {
  return Math.round((Date.now() - startTime) / 100) / 10;
}

const { XMLHttpRequest } = require('xmlhttprequest-ssl');

async function download(element, elementType, contentType='text/plain', job=theJob)
{
  let url = `http://127.0.0.1:3521/methods/download/jobs/${job}/${elementType}/${element}`;

  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();

    xhr.open("GET", url, true);
    xhr.onload = (event) => {

      if (xhr.status >= 200 && xhr.status < 300) {
        let contentType = xhr.getResponseHeader('content-type');
        if (contentType) contentType = contentType.split(';')[0];
        const data = (contentType === 'application/octet-stream') ? xhr.response : xhr.responseText;
        console.log('XHR GET:', contentType, data);
        resolve({ contentType, data });
      } else {
        reject(new Error(`HTTP Error ${xhr.status} fetching ${url}`));
      }
    };
    xhr.send();
  });
}

let sliceCount = 2;
download(0, 'slice')
  .then((o) => {
    sliceCount = Number(o.data);
    console.log('numSlices', o, sliceCount);
  });

async function main() {
  const compute = require('dcp/compute');

  const results2 = [];
  const rdsUrl = 'http://127.0.0.1:3521';
  const jobGraphId = 'Virg1';
  const baseJobId = theJob;

  const { RemoteHelper } = require('../remote-helper');
  const rdsHelper = new RemoteHelper(rdsUrl, baseJobId/*, jobGraphId*/);
  // Remote slices.
  const remoteSlices = rdsHelper.slices(sliceCount, theJob);
  // Remote workFn.
  const remoteWorkFn = rdsHelper.workFn();
  // Remote args.
  const remoteMlData = rdsHelper.arguments([ 'array', 'stride' ]);

  const job = compute.for(remoteSlices, remoteWorkFn, remoteMlData);
	
  job.collateResults = false;
  job.estimationSlices = Infinity;
  job.greedyEstimation = true;

  job.requires(['dcp-decode/decode']);
  job.requires(['dcp-matrix/matrix']);
	
  startTime = Date.now();

  job.computeGroups.push({joinKey: 'demo', joinSecret: 'dcp'});
  //job.computeGroups = [{joinKey: 'tdsim', joinSecret: 'dcp'}];
  //job.computeGroups.push({joinKey: 'magna', joinHash: 'eh1-61401e7eaaedc32f517c6da074dba842cb67738b0eaac1340ff51643c4f9a419'});

  job.public = { name: "Paul's RDS Matrix Multiplication" };
  job.public.description = process.env.USER + ' ' + new Date();
  job.on('accepted',           () => console.log('1:Job accepted:', job.id) );
  job.on('complete',   (complete) => console.log(`2:Job completed ${complete.length} slices, in ${timeSpan()}s`) );
  job.on('console', ({ message }) => console.log('3:', message) );
  job.on('error',         (error) => console.log('4:', error) );
  job.on('readystatechange', (rs) => console.log('5:Ready state:', rs) );
  job.on('status',       (status) => console.log('6:Status:', status) );
  job.on('result',  (resultEvent) => {
    const result = JSON.parse(resultEvent.result)['href'];
    results2.push(result);
    console.log(`result:url ${result} for slice ${resultEvent.sliceNumber}: ts ${timeSpan()}s`);
  });

  // Setup remote result storage on RDS.
  rdsHelper.resultStorage(job);

  const rslts = await job.exec(compute.marketValue);
  // OR
  //const rslts = await job.localExec(8);

  const results1 = rslts.values();
  console.log('results1', results1.length, results1.slice(0, 15));
  console.log('results2', results2.length, results2.slice(0, 15));
  console.log('Done!!');
}
require('dcp-client')
  .init(scheduler)
  .then(main)
  .finally(() => setImmediate(process.exit));
