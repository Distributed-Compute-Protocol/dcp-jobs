
//require('./cjs2-node');
const { Matrix32, groupBase64Columns, groupColumns } = require('matrix/matrix');
const { typedArrayToEncodedBase64 } = require('decode/decode');

const TEST = true;
const USE_FIXED_MXM = false;

const numGroups = 19; // NOTE: # of sandboxes -- please edit appropriately
const theJob = 77;
/** @type {number} */
let max1;
/** @type {number} */
let max2;
/** @type {Matrix32} */
let m1
/** @type {Matrix32} */
let m2;
if (USE_FIXED_MXM) {
  max1 = max2 = 2;
  ({ m1, m2 } = Matrix32.generateFixedMxMExample());
} else {
  max1 = max2 = 512;
  ({ m1, m2 } = Matrix32.generateMxMExample(max1, max2));
}

let start = Date.now();
const m3 = m1.mm(m2);
console.log(`Standalone MxM dt: ${m3.maxRow} x ${m3.maxCol}: ts ${Date.now() - start}ms`);
console.log('---------------------------------------------------------------');

function workFn(slice, array, stride) {
  progress();
  //console.log('workFn1:', slice.length, array.length, stride);
  const { base64ToUint8Array } = require('decode');
  const { Matrix32 } = require('matrix');

  progress();
  array = base64ToUint8Array(array);
  array = new Float32Array(array.buffer);
  const matrix = new Matrix32(stride, array);

  progress();
  slice = base64ToUint8Array(slice);
  slice = new Float32Array(slice.buffer);
  const matrixColumnGroup = new Matrix32(matrix.maxCol, slice);
  //console.log('workFn2:', slice.length, array.length, stride);

  const _start = Date.now();
  progress();
  const result = matrix.mm(matrixColumnGroup);
  progress();
  console.log('dt_done', Date.now() - _start);

  return result.array;
}

const XMLHttpRequest = require('xmlhttprequest-ssl');
let outCnt = 0;

function upload(str, element, elementType, contentType='text/plain'/*'application/octet-stream'*/, job=theJob)
{
  let url = 'http://127.0.0.1:3521/methods/upload';

  const xhr = new XMLHttpRequest();
  xhr.open("POST", url, true);
  xhr.onload = (event) => {
    if (outCnt++ < 15) console.log(xhr.status, xhr.responseText);
  };
  const body = `element=${element}&contentType=${contentType}&job=${job}&elementType=${elementType}&content=${str}`;
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhr.send(body);
}

// workFn
upload(workFn.toString(), 0, 'workFn');

// args
const b64 = typedArrayToEncodedBase64(m1.array);
if (TEST)
{
  const enc_b64 = decodeURIComponent(b64);
  const { base64ToUint8Array } = require('decode');
  const b8 = base64ToUint8Array(enc_b64);
  const ta = new Float32Array(b8.buffer);
  console.log('m1', m1.array.length, b64.length, ta.length);
}
console.log('Upload args', m1.array.length, b64.length); //, b64);
upload(b64, 'array', 'arg');
upload(m1.stride.toString(), 'stride', 'arg');

// slices
const base64Slices = groupBase64Columns(m2.array, max2, max1, numGroups);
let sum = 0;
for (const base64Slice of base64Slices) {
  sum += base64Slice.length;
  console.log('base64Slice.length', base64Slice.length, sum);
}

const slices = groupColumns(m2.array, max2, max1, numGroups);
let _m2 = new Matrix32(m2.maxRow, slices[0]);
console.log('A group of Matrix32', m1.maxRow, m1.maxCol, _m2.maxRow, _m2.maxCol);
start = Date.now();
const _m3 = m1.mm(_m2);
console.log(`_m3 dt: ${_m3.maxRow} x ${_m3.maxCol}: ts ${Date.now() - start}ms`);
console.log('---------------------------------------------------------------');

upload(base64Slices.length.toString(), 0, 'slice');
for (let k = 0; k < base64Slices.length; k++) {
  const slice = base64Slices[k];
  if (k < 15) console.log('Upload slice:', slice.length); //, slice);
  upload(slice, k+1, 'slice');
}
