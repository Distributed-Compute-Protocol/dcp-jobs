
//const debugging = require('dcp/debugging').scope('worker');
function debugging() { return true; }

// @ts-check
'use strict';

class RemoteHelper {
  /**
   * @param {string} rdsUrl               -- Url for remote data storage.
   * @param {number|string} [baseJobId=1] -- Default jobId, used to distinguish nodes of the job graph.
   * @param {number|string} [jobGraphId]  -- Optional identifier used to group a set of jobs.
   */
  constructor(rdsUrl, baseJobId=1, jobGraphId) {
    this.compute = require('dcp/compute');
    /** @type {string} */
    this.rdsUrl = rdsUrl;
    /** @type {number|string} */
    this.baseJobId = baseJobId;
    if (jobGraphId) {
      /** @type {number|string} */
      this.jobGraphId = jobGraphId;
      /** @type {string} */
      this.rdsDownloadUrl = `${rdsUrl}/methods/download/jobs/${jobGraphId}`;
      /** @type {string} */
      this.rdsUploadUrl = `${rdsUrl}/methods/upload/jobs/${jobGraphId}`;
    } else {
      /** @type {string} */
      this.rdsDownloadUrl = `${rdsUrl}/methods/download/jobs`;
      /** @type {string} */
      this.rdsUploadUrl = `${rdsUrl}/methods/upload/jobs`;
    }
    debugging() && console.debug(`RemoteHelper.ctor: baseJobId ${this.baseJobId}, rdsDownloadUrl ${this.rdsDownloadUrl}, rdsUploadUrl ${this.rdsUploadUrl}`);
  }
  /**
   * Construct RemoteDataPattern for remote slices.
   * @param {number} sliceCount     -- Number of slices.
   * @param {number|string} [jobId] -- Optional job id, used to distinguish nodes of the job graph.
   * @returns {object}
   */
  slices(sliceCount, jobId) {
    if (!jobId) jobId = this.baseJobId;
    const sliceUrl = `${this.rdsDownloadUrl}/${jobId}/slice/{slice}`;
    debugging() && console.debug(`RemoteHelper.slices: sliceCount ${sliceCount}, sliceUrl ${sliceUrl}`);
    const remoteSlices = new this.compute.RemoteDataPattern(sliceUrl, sliceCount);
    return remoteSlices;
  }
  /**
   * Construct URL for remote work function.
   * @param {number|string} [jobId]     -- Optional job id, used to distinguish nodes of the job graph.
   * @param {number|string} [element=0] -- Optional last node in workFn storage path.
   * @returns {URL} 
   */
  workFn(jobId, element=0) {
    if (!jobId) jobId = this.baseJobId;
    const workFnUrl = `${this.rdsDownloadUrl}/${jobId}/workFn/${element}`;
    debugging() && console.debug(`RemoteHelper.workFn: workFnUrl ${workFnUrl}`);
    const remoteWorkFn = new URL(workFnUrl);
    return remoteWorkFn;
  }
  /**
   * Given an array of argument names, construct remote arguments.
   * @param {string[]|number[]} argNames -- Array of argument names.
   * @param {number|string} [jobId]      -- Optional job id, used to distinguish nodes of the job graph.
   * @returns {object}
   */
  arguments(argNames, jobId) {
    if (!jobId) jobId = this.baseJobId;
    const argUrl = `${this.rdsDownloadUrl}/${jobId}/arg/`;
    debugging() && console.debug(`RemoteHelper.arguments: argNames ${argNames}, argUrl ${argUrl}`);
    argNames = argNames.map(name => `${argUrl}${name}`);
    const remoteMlData = new this.compute.RemoteDataSet(argNames);
    return remoteMlData;
  }
  /**
   * Set up remote result storage on RDS.
   * @param {object}        job     -- The job handle; return value of compute.for .
   * @param {number|string} [jobId] -- Optional job id, used to distinguish nodes of the job graph.
   * @param {string}        [contentType='application/json'] -- Optional content type of remote storage; default: application/json.
   */
  resultStorage(job, jobId, contentType='application/json') {
    if (!jobId) jobId = this.baseJobId;
    const resultUrl = `${this.rdsUploadUrl}/${jobId}/`;
    debugging() && console.debug(`RemoteHelper.resultStorage: contentType ${contentType}, resultUrl ${resultUrl}`);
    const remoteStorageParams = {
      job         : jobId,
      elementType : 'result',
      contentType : contentType
    };
    if (this.jobGraphId)
      remoteStorageParams['jobGraphId'] = this.jobGraphId;
    job.setResultStorage(new URL(resultUrl), remoteStorageParams);
  }
}
exports.RemoteHelper = RemoteHelper;
