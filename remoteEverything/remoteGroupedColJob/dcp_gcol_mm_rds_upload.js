
//require('./cjs2-node');
const { Matrix32 } = require('matrix/matrix');
const { typedArrayToEncodedBase64 } = require('decode/decode');

const TEST = true;
const USE_FIXED_MXM = false;

const numGroups = 19; // NOTE: # of sandboxes -- please edit appropriately
const theJob = 77;
/** @type {number} */
let max1;
/** @type {number} */
let max2;
/** @type {Matrix32} */
let m1
/** @type {Matrix32} */
let m2;
if (USE_FIXED_MXM) {
  max1 = max2 = 2;
  ({ m1, m2 } = Matrix32.generateFixedMxMExample());
} else {
  max1 = max2 = 512;
  ({ m1, m2 } = Matrix32.generateMxMExample(max1, max2));
}

let start = Date.now();
const m3 = m1.mm(m2);
console.log(`Standalone MxM dt: ${m3.maxRow} x ${m3.maxCol}: ts ${Date.now() - start}ms`);
console.log('---------------------------------------------------------------');

function workFn(slice, array, stride) {
  //console.log('workFn1:', slice.length, array.length, stride);
  const { base64ToUint8Array } = require('decode');
  const { Matrix32 } = require('matrix');

  array = base64ToUint8Array(array);
  array = new Float32Array(array.buffer);
  const matrix = new Matrix32(stride, array);

  const result = [];
  const _start = Date.now();
  for (const column of slice) {
    progress();
    const u8 = base64ToUint8Array(column);
    const f32 = new Float32Array(u8.buffer);
    result.push(matrix.mv(f32));
  }
  console.log('dt_done', Date.now() - _start);

  return result;  
}

const XMLHttpRequest = require('xmlhttprequest-ssl');
let outCnt = 0;

function upload(str, element, elementType, contentType='text/plain'/*'application/octet-stream'*/, job=theJob)
{
  let url = 'http://127.0.0.1:3521/methods/upload';

  const xhr = new XMLHttpRequest();
  xhr.open("POST", url, true);
  xhr.onload = (event) => {
    if (outCnt++ < 15) console.log(xhr.status, xhr.responseText);
  };
  const body = `element=${element}&contentType=${contentType}&job=${job}&elementType=${elementType}&content=${str}`;
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhr.send(body);
}

// workFn
upload(workFn.toString(), 0, 'workFn');

// args
const b64 = typedArrayToEncodedBase64(m1.array);
if (TEST)
{
  const enc_b64 = decodeURIComponent(b64);
  const { base64ToUint8Array } = require('decode');
  const b8 = base64ToUint8Array(enc_b64);
  const ta = new Float32Array(b8.buffer);
  console.log('m1', m1.array.length, b64.length, ta.length);
}
console.log('Upload args', m1.array.length, b64.length); //, b64);
upload(b64, 'array', 'arg');
upload(m1.stride.toString(), 'stride', 'arg');

// slices
function group(array, maxGroupSize) {
  const output = [];
  let group = [];
  for (const element of array) {
    group.push(element);
    if (group.length >= maxGroupSize) {
      output.push(group);
      groupCount = 0;
      group = [];
    }
  }
  if (group.length > 0)
    output.push(group);
  return output;
}

const m2_columns = m2.toColumns();
const m2_grouped_columns = group(m2_columns, Math.ceil(m2_columns.length / numGroups));
console.log(`Grouped columns(m2_grouped_columns.length=${m2_grouped_columns.length}: numgroups`, numGroups, Math.ceil(m2_columns.length / numGroups), m2_columns.length);

let _m2_grouped_column = m2_grouped_columns[0], _rslt = [];
console.log('A group of columns of Matrix32', _m2_grouped_column.length, _m2_grouped_column[0].length);
start = Date.now();
for (const _m2_column of _m2_grouped_column) {
  const _m3 = m1.mv(_m2_column);
  _rslt.push(_m3);
}
console.log(`_m3 dt: ${_rslt[0].length} x ${_rslt.length}:`, Date.now() - start);
console.log('---------------------------------------------------------------');

upload(m2_grouped_columns.length.toString(), 0, 'slice');
for (let k = 0; k < m2_grouped_columns.length; k++) {
  const column_group = m2_grouped_columns[k];
  /*const encoded_cg = [];
  for (const column of column_group) {
    console.log('FOO', typeof column, column.constructor.name);
    const encoded_column = typedArrayToEncodedBase64(column);
    encoded_cg.push(encoded_column)
  }
  const slice = JSON.stringify(encoded_cg);*/
  const encoded_cg = column_group.map((column) => typedArrayToEncodedBase64(column));
  const slice = JSON.stringify(encoded_cg);
  if (k < 15) console.log('Upload slice:', slice.length); //, slice);
  upload(slice, k+1, 'slice', 'application/json');
}
