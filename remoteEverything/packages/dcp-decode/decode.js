//
//  Read numpy array into a typed array.
//  Used the numpy spec https://numpy.org/doc/stable/reference/generated/numpy.lib.format.html .
//  Supports numpy v1 and v2; v3 is for a utf8 header and is not supported yet except for the 0 -> 0xFF subset, of course.
//  Grabbed the hdrStr replace strings from https://github.com/propelml/tfjs-npy/blob/master/npy.ts
//  pfr
//  ChrisM
//  Nov 1, 2021
//
module.declare([], function(require, exports, module) {

  function uint8ToString(b8, offset, length) {
    let str = "";
    for (let k = offset; k < offset + length; k++)
      str += String.fromCharCode(b8[k]);
    return str;
  }
  function b64ToUint6 (nChr) {
    return nChr > 64 && nChr < 91  ? nChr - 65
         : nChr > 96 && nChr < 123 ? nChr - 71
         : nChr > 47 && nChr < 58  ? nChr + 4
         : nChr === 43 ? 62
         : nChr === 47 ? 63 : 0;
  }
  exports.base64ToUint8Array = function _base64ToUint8Array(b64) {
    if (!b64) throw new Error('base64ToUint8Array: base64 input string is undefined.');
    const sB64Enc = b64.replace(/[^A-Za-z0-9+/]/g, ""), nInLen = sB64Enc.length; b64 = null;
    const nOutLen = (nInLen * 3 + 1) >> 2, taBytes = new Uint8Array(nOutLen);
    for (let nMod3, nMod4, nUint24 = 0, nOutIdx = 0, nInIdx = 0; nInIdx < nInLen; nInIdx++) {
      nMod4 = nInIdx & 3;
      nUint24 |= b64ToUint6(sB64Enc.charCodeAt(nInIdx)) << (6 * (3 - nMod4));
      if (nMod4 === 3 || nInLen - nInIdx === 1) {
        for (nMod3 = 0; nMod3 < 3 && nOutIdx < nOutLen; nMod3++, nOutIdx++)
          taBytes[nOutIdx] = (nUint24 >>> ((16 >>> nMod3) & 24)) & 255;
        nUint24 = 0;
      }
    }
    return taBytes;
  }
  exports.base64ToFloat32Array = function _base64ToFloat32Array(b64) {
    //console.log('base64ToFloat32Array1: ', typeof b64);
    const b8 = exports.base64ToUint8Array(b64);
    b64 = null;
    //console.log('base64ToFloat32Array2: ', typeof b8);
    return new Float32Array(b8.buffer);
  }
  exports.npyBase64ToTypedArray = function _npyBase64ToTypedArray(b64) {
    //console.log('npyBase64ToTypedArray1: ', typeof b64);
    const b8 = exports.base64ToUint8Array(b64);
    b64 = null;
    //console.log('npyBase64ToTypedArray2: ', typeof b8);
    return exports.npyBytesToShapedArray(b8).typedArray;
  }
  exports.createTypedArray = function _createTypedArray(b8, descr) {
    switch (+descr[2] | 0) {
      case 1: return (descr[1] === 'u') ? new Uint8Array(b8.buffer) : new Int8Array(b8.buffer);
      case 2: return (descr[1] === 'u') ? new Uint16Array(b8.buffer) : new Int16Array(b8.buffer);
      case 4: return (descr[1] === 'f') ? new Float32Array(b8.buffer) : (descr[1] === 'u') ? new Uint32Array(b8.buffer) : new Int32Array(b8.buffer);
      case 8: return (descr[1] === 'f') ? new Float64Array(b8.buffer) : (descr[1] === 'u') ? new BigUint64Array(b8.buffer) : new BigInt64Array(b8.buffer);
      default: throw new Error(`Unknown numpy descriptor ${descr}`);
    }
  }
  exports.npyBytesToShapedArray = function _npyBytesToShapedArray(b8) {
    // Verify NumPy magic bytes.
    const npHdr = [ 0x93, 0x4e, 0x55, 0x4d, 0x50, 0x59, 0x1, 0x0 ];
    let offset = npHdr.length;
    for (let k = 0; k < offset; k++) {
      if (b8[k] !== npHdr[k])
        throw new Error(`The numpy header byte # ${k} should be ${npHdr[k]}, not ${b8[k]}`);
    }

    // Compute header length.
    const shortHdr = b8[offset + 2] === 0x7b; // Check whether hdr can fit in 64K .
    const dv = new DataView(b8.buffer, offset, 4);
    let hdrLen;
    if (shortHdr) {
      hdrLen = dv.getInt16(0, true);
      offset += 2;
    } else {
      hdrLen = dv.getInt32(0, true);
      offset += 4;
    }

    // Parse dictionary portion of header as JSON.
    const hdrStr = uint8ToString(b8, offset, hdrLen);
    const hdrJson = hdrStr.replace("True", "true")
                          .replace("False", "false")
                          .replace(/'/g, `"`)
                          .replace(/,\s*}/, " }")
                          .replace(/,?\)/, "]")
                          .replace("(", "[");
    //console.log('npyBytesToShapedArray: hdrJson: ', hdrJson)
    const { descr, fortran_order, shape } = JSON.parse(hdrJson);

    // Check for unsupported fortran_order.
    if (fortran_order)
      throw Error('numPyToTypedArray does not support fortran_order yet. Implement it now!');

    // Compute # of elements.
    let totalSize = shape.length > 0 ? shape[0] : 1;
    for (let k = 1; k < shape.length; k++)
      totalSize *= shape[k];

    // Create typedArray.
    offset += hdrLen;
    const truncatedTypedArray = b8.slice(offset);
    b8_byteLength = b8.byteLength;
    b8 = null;
    const typedArray = exports.createTypedArray(truncatedTypedArray, descr);

    // Sanity check.
    totalSize *= typedArray.BYTES_PER_ELEMENT;
    if (b8_byteLength !== totalSize + offset || totalSize != typedArray.byteLength)
      throw new Error(`Inconsistent sizes in parsing numpy array ${b8_byteLength}, ${totalSize}, ${typedArray.byteLength}, ${offset}`);

    return { typedArray: typedArray, dtype: descr, shape: shape };
  }

});
