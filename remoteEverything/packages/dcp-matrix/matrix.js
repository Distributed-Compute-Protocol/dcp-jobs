

if (!assert) {
  function assert(b) {
    //if (!b) console.trace('Assert Failed!');
  }
}

module.declare([], function(require, exports, module) {
  /**
   * This function groups columns of ta so that the # of groups is no more than numGroups,
   * then it translates the columns to base64 and applies encodeURIComponent.
   * 
   * E.g. When columnLength = numColumns = 100, numGroups = 12, the returned array base64Groups,
   * will have 12 elements each with 9 columns except the last which has 1 column.
   * 
   * @param {TypedArray} ta       - ta represents a column-major matrix with stride columnLength
   * @param {number} columnLength - number of rows of matrix ta
   * @param {number} numColumns   - number of columns of matrix ta
   * @param {number} numGroups    - desired # of groups (may be less)
   * @returns {string[]}
   */
  exports.groupBase64Columns = function groupBase64Columns(ta, columnLength, numColumns, numGroups) {
    const countPerGroup = Math.ceil(numColumns / numGroups);
    const sliceCountFloor = Math.floor(numColumns / countPerGroup);
    //console.log(`groupBase64Columns: sliceCount ${Math.ceil(numColumns / countPerGroup)}, countPerGroup ${countPerGroup}, numgroups ${numGroups}`);
    const base64Groups = [];
    let offset = 0, bpe = ta.BYTES_PER_ELEMENT;
    for (let k = 0; k < sliceCountFloor; k++) {
      const bufferGroup = Buffer.from(ta.buffer, bpe*offset, bpe*countPerGroup*columnLength);
      const base64Group = bufferGroup.toString('base64');
      const encodedBase64Group = encodeURIComponent(base64Group);
      base64Groups.push(encodedBase64Group);
      offset += countPerGroup * columnLength;
    }
    if (offset < ta.length) {
      const bufferGroup = Buffer.from(ta.buffer, bpe*offset, bpe*(ta.length - offset));
      const base64Group = bufferGroup.toString('base64');
      const encodedBase64Group = encodeURIComponent(base64Group);
      base64Groups.push(encodedBase64Group);
    }
    return base64Groups;
  }
  /**
   * This function groups columns of ta so that the # of groups is no more than numGroups,
   * 
   * E.g. When columnLength = numColumns = 100, numGroups = 12, the returned array base64Groups,
   * will have 12 elements each with 9 columns except the last which has 1 column.
   * 
   * @param {Float32Array} ta       - ta represents a column-major matrix with stride columnLength
   * @param {number} columnLength - number of rows of matrix ta
   * @param {number} numColumns   - number of columns of matrix ta
   * @param {number} numGroups    - desired # of groups (may be less)
   * @returns {Float32Array}
   */
  exports.groupColumns = function groupColumns(ta, columnLength, numColumns, numGroups) {
    const countPerGroup = Math.ceil(numColumns / numGroups);
    const sliceCountFloor = Math.floor(numColumns / countPerGroup);
    //console.log(`groupColumns: sliceCount ${Math.ceil(numColumns / countPerGroup)}, countPerGroup ${countPerGroup}, numgroups ${numGroups}`);
    const slices = [];
    let offset = 0, bpe = ta.BYTES_PER_ELEMENT;
    for (let k = 0; k < sliceCountFloor; k++) {
      const slice = new Float32Array(ta.buffer, bpe*offset, countPerGroup*columnLength);
      slices.push(slice);
      offset += countPerGroup * columnLength;
    }
    if (offset < ta.length) {
      const slice = new Float32Array(ta.buffer, bpe*offset, ta.length - offset);
      slices.push(slice);
    }
    return slices;
  }
  /**
   * Matrix of the form array or [].
   * Viz., a 1-dimensional array.
   */
  class Matrix { // column-major
    constructor(stride, array, isColumnMatrix) {
      if (!isColumnMatrix) { // single array folded via columns into a matrix
        this.maxRow = stride;
        if (array.length > 0) {
          this.array = array;
          assert((array.length % stride) === 0);
          this.maxCol = array.length / stride;
        } else { // ctor(stride, maxCol)
          this.maxCol = array;
          this.array = new Array(this.maxCol * stride);
        }
        assert(this.maxCol > 0 && this.maxRow > 0 && this.maxCol * this.maxRow === this.array.length);
      } else { // ctor(maxRow, maxCol) -- array of columns.
        const maxRow = stride
        const maxCol = array;
        assert(this.maxCol > 0 && this.maxRow > 0);
        if (maxCol) {
          this.maxRow = maxRow;
          this.maxCol = maxCol;
          this.array = new Array(this.maxCol);
          for (let c = 0; c < this.maxCol; c++)
            this.array[c] = new Array(this.maxRow);
        } else {
          this.array = maxRow;
          this.maxCol = this.array.length;
          assert(this.maxCol > 0);
          this.maxRow = this.array[0].length;
        }
        assert(this.maxCol === this.array.length && this.maxRow === this.array[0].length);
      }
      this.stride = this.maxRow;
    }
    get(r, c) {
      assert(0 <= r && r < this.maxRow && 0 <= c && c < this.maxCol && (r + c * this.maxRow) < this.array.length);
      return this.array[r + c * this.maxRow];
    }
    set(r, c, v) {
      assert(0 <= r && r < this.maxRow && 0 <= c && c < this.maxCol && (r + c * this.maxRow) < this.array.length);
      this.array[r + c * this.maxRow] = v;
    }
    mm(matrix) {
      assert(this.maxCol === matrix.maxRow);
      //sum(a[m,k]*b[k,n] : 0 <= k < this.maxCol)
      const output = new Matrix(this.maxRow, matrix.maxCol);
      for (let c = 0; c < matrix.maxCol; c++) {
        for (let r = 0; r < this.maxRow; r++) {
          let x = 0;
          for (let k = 0; k < this.maxCol; k++)
            x += this.get(r, k) * matrix.get(k, c);
          output.set(r, c, x);
        }
      }
      return output;
    }
    mv(array) {
      assert(this.maxCol === array.length);
      //sum(a[m,k]*b[k] : 0 <= k < this.maxCol)
      const output = new Array(this.maxRow);
      for (let r = 0; r < this.maxRow; r++) {
        let x = 0;
        for (let c = 0; c < this.maxCol; c++)
          x += this.get(r, c) * array[c];
        output[r] = x;
      }
      return output;
    }
    toColumns() {
      const output = new Array(this.maxCol);
      for (let c = 0; c < this.maxCol; c++) {
        const current = new Array(this.maxRow);
        const offset = c * this.maxRow;
        for (let r = 0; r < this.maxRow; r++)
          current[r] = this.array[r + offset];
        output[c] = current;
      }
      return output;
    }
    static fromColumns(columns) {
      assert(columns.length > 0 && columns[0].length > 0);
      const matrix = new Matrix(columns[0].length, columns.length);
      for (let c = 0; c < matrix.maxCol; c++) {
        const column = columns[c];
        for (let r = 0; r < matrix.maxRow; r++)
          matrix.set(r, c, column[r]);
      }
      return matrix;
    }	
    static _createRandom(matrix) {
      for (let c = 0; c < matrix.maxCol; c++)
        for (let r = 0; r < matrix.maxRow; r++)
          matrix.set(r, c, Math.random() + (r == c ? 1 : 0));
      return matrix;
    }
    static createRandom(numRows, numCols) {
      assert(numRows > 0 && numCols > 0);
      const matrix = new Matrix(numRows, numCols);
      return this._createRandom(matrix);
    }
    /**
     * Generates random matrices:
     *   m1 - m x n matrix
     *   m2 - n x m matrix
     * @param {number} m 
     * @param {number} n 
     * @returns {object} - { m1, m2 }
     */
    static generateMxMExample(m, n) {
      const m1 = Matrix.createRandom(m, n);
      const m2 = Matrix.createRandom(n, m);
      return { m1, m2 };
    }
    static generateFixedMxMExample() {
      const m1 = new Matrix(2, 2);
      const m2 = new Matrix(2, 2);
      const stride = m1.maxRow;
      m1.set(0, 0, 1); m1.set(0, 1, 6);
      m1.set(1, 0, 5); m1.set(1, 1, 7);
      m2.set(0, 0, 7); m2.set(0, 1, 5);
      m2.set(1, 0, 6); m2.set(1, 1, 1);
      return { m1, stride, m2 };
    }
    _mm2(matrix) {
      const columns = matrix.toColumns();
      const outputColumns = new Array(matrix.maxCol);
      for (let k = 0; k < matrix.maxCol; k++)
        outputColumns[k] = this.mv(columns[k]);
      return outputColumns;
    }
    mm2(matrix) {
      return Matrix.fromColumns(this._mm2(matrix));
    }
    dump() {
      for (let r = 0; r < this.maxRow; r++) {
        let s = '';
        for (let c = 0; c < this.maxCol; c++)
          s += `${this.get(r, c)}, `;
        console.log(s);
      }
    }
  }
  exports.Matrix = Matrix;
  /**
   * Matrix of the form array of array or [][].
   * Viz., a 2-dimensional array.
   */
  class ColumnMatrix extends Matrix { // column-major
    constructor(maxRow, maxCol) {
      super(maxRow, maxCol, true /*isColumnMatrix*/);
      assert(this.maxCol > 0 && this.maxRow > 0);
      assert(this.maxCol === this.array.length && this.maxRow === this.array[0].length);
    }
    get(r, c) {
      assert(0 <= r && r < this.maxRow && 0 <= c && c < this.maxCol);
      return this.array[c][r];
    }
    set(r, c, v) {
      assert(0 <= r && r < this.maxRow && 0 <= c && c < this.maxCol);
      this.array[c][r] = v;
    }
    toColumns() {
      return this.array;
    }
    static fromColumns(columns) {
      assert(columns.length > 0 && columns[0].length > 0);
      return new ColumnMatrix(columns);
    }
    static createRandom(numRows, numCols) {
      assert(numRows > 0 && numCols > 0);
      const matrix = new ColumnMatrix(numRows, numCols);
      return this._createRandom(matrix);
    }
    mm(matrix) {
      assert(matrix instanceof ColumnMatrix);
      return ColumnMatrix.fromColumns(this._mm2(matrix));
    }
  }
  exports.ColumnMatrix = ColumnMatrix;
  /**
   * Matrix of the form Float32Array.
   * Viz., a 1-dimensional array.
   */
    class Matrix32 { // column-major
    constructor(stride, array, isColumnMatrix) {
      if (!isColumnMatrix) { // single array folded via columns into a matrix
        this.maxRow = stride;
        if (array.length > 0) {
          assert(array instanceof Float32Array);
          this.array = array;
          assert((array.length % stride) === 0);
          this.maxCol = array.length / stride;
        } else { // ctor(stride, maxCol)
          this.maxCol = array;
          this.array = new Float32Array(this.maxCol * stride);
        }
        assert(this.maxCol > 0 && this.maxRow > 0 && this.maxCol * this.maxRow === this.array.length);
      } else { // ctor(maxRow, maxCol) -- array of columns.
        const maxRow = stride
        const maxCol = array;
        if (maxCol) {
          this.maxRow = maxRow;
          this.maxCol = maxCol;
          assert(this.maxCol > 0 && this.maxRow > 0);
          this.array = new Array(this.maxCol);
          for (let c = 0; c < this.maxCol; c++)
            this.array[c] = new Float32Array(this.maxRow);
        } else {
          this.array = maxRow;
          this.maxCol = this.array.length;
          assert(this.maxCol > 0);
          this.maxRow = this.array[0].length;
        }
        assert(this.maxCol === this.array.length && this.maxRow === this.array[0].length);
      }
      this.stride = this.maxRow;
    }
    get(r, c) {
      assert(0 <= r && r < this.maxRow && 0 <= c && c < this.maxCol && (r + c * this.maxRow) < this.array.length);
      return this.array[r + c * this.maxRow];
    }
    set(r, c, v) {
      assert(0 <= r && r < this.maxRow && 0 <= c && c < this.maxCol && (r + c * this.maxRow) < this.array.length);
      this.array[r + c * this.maxRow] = Math.fround(v);
    }
    mm(matrix) {
      assert(this.maxCol === matrix.maxRow);
      //sum(a[m,k]*b[k,n] : 0 <= k < this.maxCol)
      const output = new Matrix32(this.maxRow, matrix.maxCol);
      for (let c = 0; c < matrix.maxCol; c++) {
        for (let r = 0; r < this.maxRow; r++) {
          let x = 0;
          for (let k = 0; k < this.maxCol; k++)
            x += this.get(r, k) * matrix.get(k, c);
          output.set(r, c, x);
        }
      }
      return output;
    }
    mv(array) {
      //const _st = Date.now();
      //console.log('Matrix32.mv(array', this.maxCol, this.maxRow, array.length);
      assert(this.maxCol === array.length);
      //sum(a[m,k]*b[k] : 0 <= k < this.maxCol)
      const output = new Float32Array(this.maxRow);
      for (let r = 0; r < this.maxRow; r++) {
        let x = 0;
        for (let c = 0; c < this.maxCol; c++)
          x += this.get(r, c) * array[c];
        output[r] = x;
      }
      //console.log('Matrix32.mv: dt=', Date.now() - _st);
      return output;
    }
    toColumns() {
      //console.log('Matrix32.toColumns', this);
      const output = new Array(this.maxCol);
      for (let c = 0; c < this.maxCol; c++) {
        const current = new Float32Array(this.maxRow);
        const offset = c * this.maxRow;
        for (let r = 0; r < this.maxRow; r++) {
          current[r] = this.array[r + offset];
          //console.log('...', current[r], this.array[r + offset]);
        }
        output[c] = current;
      }
      //console.log('...2', output);
      return output;
    }
    static fromColumns(columns) {
      assert(columns.length > 0 && columns[0].length > 0);
      const matrix = new Matrix32(columns[0].length, columns.length);
      for (let c = 0; c < matrix.maxCol; c++) {
        const column = columns[c];
        for (let r = 0; r < matrix.maxRow; r++)
          matrix.set(r, c, column[r]);
      }
      return matrix;
    }	
    static _createRandom(matrix) {
      for (let c = 0; c < matrix.maxCol; c++)
        for (let r = 0; r < matrix.maxRow; r++)
          matrix.set(r, c, Math.random() + (r == c ? 1 : 0));
      return matrix;
    }
    static createRandom(numRows, numCols) {
      assert(numRows > 0 && numCols > 0);
      const matrix = new Matrix32(numRows, numCols);
      return this._createRandom(matrix);
    }
    /**
     * Generates random matrices:
     *   m1 - m x n matrix
     *   m2 - n x m matrix
     * @param {number} m 
     * @param {number} n 
     * @returns {object} - { m1, m2 }
     */
     static generateMxMExample(m, n) {
      const m1 = Matrix32.createRandom(m, n);
      const m2 = Matrix32.createRandom(n, m);
      return { m1, m2 };
    }
    static generateFixedMxMExample() {
      const m1 = new Matrix32(2, 2);
      const m2 = new Matrix32(2, 2);
      const stride = m1.maxRow;
      m1.set(0, 0, 1); m1.set(0, 1, 6);
      m1.set(1, 0, 5); m1.set(1, 1, 7);
      m2.set(0, 0, 7); m2.set(0, 1, 5);
      m2.set(1, 0, 6); m2.set(1, 1, 1);
      return { m1, stride, m2 };
    }
    _mm2(matrix) {
      const columns = matrix.toColumns();
      //console.log('Matrix32._mm2', matrix, columns);
      const outputColumns = new Array(matrix.maxCol);
      for (let k = 0; k < matrix.maxCol; k++)
        outputColumns[k] = this.mv(columns[k]);
      return outputColumns;
    }
    mm2(matrix) {
      return Matrix32.fromColumns(this._mm2(matrix));
    }
    dump() {
      for (let r = 0; r < this.maxRow; r++) {
        let s = '';
        for (let c = 0; c < this.maxCol; c++)
          s += `${this.get(r, c)}, `;
        console.log(s);
      }
    }
  }
  exports.Matrix32 = Matrix32;
  /**
   * Matrix of the form Float32Array of Float32Array or Float32Array[Float32Array].
   * Viz., a 2-dimensional array.
   */
  class ColumnMatrix32 extends Matrix32 { // column-major
    constructor(maxRow, maxCol) {
      super(maxRow, maxCol, true /*isColumnMatrix*/);
      assert(this.maxCol > 0 && this.maxRow > 0);
      assert(this.maxCol === this.array.length && this.maxRow === this.array[0].length);
    }
    get(r, c) {
      assert(0 <= r && r < this.maxRow && 0 <= c && c < this.maxCol);
      return this.array[c][r];
    }
    set(r, c, v) {
      assert(0 <= r && r < this.maxRow && 0 <= c && c < this.maxCol);
      this.array[c][r] = Math.fround(v);
    }
    toColumns() {
      return this.array;
    }
    static fromColumns(columns) {
      assert(columns.length > 0 && columns[0].length > 0);
      return new ColumnMatrix32(columns);
    }
    static createRandom(numRows, numCols) {
      assert(numRows > 0 && numCols > 0);
      const matrix = new ColumnMatrix32(numRows, numCols);
      return this._createRandom(matrix);
    }
    mm(matrix) {
      assert(matrix instanceof ColumnMatrix32);
      return ColumnMatrix32.fromColumns(this._mm2(matrix));
    }
  }
  exports.ColumnMatrix32 = ColumnMatrix32;
  /**
   * Matrix of the form Float64Array.
   * Viz., a 1-dimensional array.
   */
  class Matrix64 { // column-major
    constructor(stride, array, isColumnMatrix) {
      if (!isColumnMatrix) { // single array folded via columns into a matrix
        this.maxRow = stride;
        if (array.length > 0) {
          assert(array instanceof Float64Array);
          this.array = array;
          assert((array.length % stride) === 0);
          this.maxCol = array.length / stride;
        } else { // ctor(stride, maxCol)
          this.maxCol = array;
          this.array = new Float64Array(this.maxCol * stride);
        }
        assert(this.maxCol > 0 && this.maxRow > 0 && this.maxCol * this.maxRow === this.array.length);
      } else { // ctor(maxRow, maxCol) -- array of columns.
        const maxRow = stride
        const maxCol = array;
        assert(this.maxCol > 0 && this.maxRow > 0);
        if (maxCol) {
          this.maxRow = maxRow;
          this.maxCol = maxCol;
          this.array = new Array(this.maxCol);
          for (let c = 0; c < this.maxCol; c++)
            this.array[c] = new Float64Array(this.maxRow);
        } else {
          this.array = maxRow;
          this.maxCol = this.array.length;
          assert(this.maxCol > 0);
          this.maxRow = this.array[0].length;
        }
        assert(this.maxCol === this.array.length && this.maxRow === this.array[0].length);
      }
      this.stride = this.maxRow;
    }
    get(r, c) {
      assert(0 <= r && r < this.maxRow && 0 <= c && c < this.maxCol && (r + c * this.maxRow) < this.array.length);
      return this.array[r + c * this.maxRow];
    }
    set(r, c, v) {
      assert(0 <= r && r < this.maxRow && 0 <= c && c < this.maxCol && (r + c * this.maxRow) < this.array.length);
      this.array[r + c * this.maxRow] = v;
    }
    mm(matrix) {
      assert(this.maxCol === matrix.maxRow);
      //sum(a[m,k]*b[k,n] : 0 <= k < this.maxCol)
      const output = new Matrix64(this.maxRow, matrix.maxCol);
      for (let c = 0; c < matrix.maxCol; c++) {
        for (let r = 0; r < this.maxRow; r++) {
          let x = 0;
          for (let k = 0; k < this.maxCol; k++)
            x += this.get(r, k) * matrix.get(k, c);
          output.set(r, c, x);
        }
      }
      return output;
    }
    mv(array) {
      assert(this.maxCol === array.length);
      //sum(a[m,k]*b[k] : 0 <= k < this.maxCol)
      const output = new Float64Array(this.maxRow);
      for (let r = 0; r < this.maxRow; r++) {
        let x = 0;
        for (let c = 0; c < this.maxCol; c++)
          x += this.get(r, c) * array[c];
        output[r] = x;
      }
      return output;
    }
    toColumns() {
      const output = new Array(this.maxCol);
      for (let c = 0; c < this.maxCol; c++) {
        const current = new Float64Array(this.maxRow);
        const offset = c * this.maxRow;
        for (let r = 0; r < this.maxRow; r++)
          current[r] = this.array[r + offset];
        output[c] = current;
      }
      return output;
    }
    static fromColumns(columns) {
      assert(columns.length > 0 && columns[0].length > 0);
      const matrix = new Matrix64(columns[0].length, columns.length);
      for (let c = 0; c < matrix.maxCol; c++) {
        const column = columns[c];
        for (let r = 0; r < matrix.maxRow; r++)
          matrix.set(r, c, column[r]);
      }
      return matrix;
    }	
    static _createRandom(matrix) {
      for (let c = 0; c < matrix.maxCol; c++)
        for (let r = 0; r < matrix.maxRow; r++)
          matrix.set(r, c, Math.random() + (r == c ? 1 : 0));
      return matrix;
    }
    static createRandom(numRows, numCols) {
      assert(numRows > 0 && numCols > 0);
      const matrix = new Matrix64(numRows, numCols);
      return this._createRandom(matrix);
    }
    /**
     * Generates random matrices:
     *   m1 - m x n matrix
     *   m2 - n x m matrix
     * @param {number} m 
     * @param {number} n 
     * @returns {object} - { m1, m2 }
     */
    static generateMxMExample(m, n) {
      const m1 = Matrix64.createRandom(m, n);
      const m2 = Matrix64.createRandom(n, m);
      return { m1, m2 };
    }
    static generateFixedMxMExample() {
      const m1 = new Matrix64(2, 2);
      const m2 = new Matrix64(2, 2);
      const stride = m1.maxRow;
      m1.set(0, 0, 1); m1.set(0, 1, 6);
      m1.set(1, 0, 5); m1.set(1, 1, 7);
      m2.set(0, 0, 7); m2.set(0, 1, 5);
      m2.set(1, 0, 6); m2.set(1, 1, 1);
      return { m1, stride, m2 };
    }
    _mm2(matrix) {
      const columns = matrix.toColumns();
      const outputColumns = new Array(matrix.maxCol);
      for (let k = 0; k < matrix.maxCol; k++)
        outputColumns[k] = this.mv(columns[k]);
      return outputColumns;
    }
    mm2(matrix) {
      return Matrix64.fromColumns(this._mm2(matrix));
    }
    dump() {
      for (let r = 0; r < this.maxRow; r++) {
        let s = '';
        for (let c = 0; c < this.maxCol; c++)
          s += `${this.get(r, c)}, `;
        console.log(s);
      }
    }
  }
  exports.Matrix64 = Matrix64;
  /**
   * Matrix of the form Float64Array of Float64Array or Float64Array[Float64Array].
   * Viz., a 2-dimensional array.
   */
  class ColumnMatrix64 extends Matrix64 { // column-major
    constructor(maxRow, maxCol) {
      super(maxRow, maxCol, true /*isColumnMatrix*/);
      assert(this.maxCol > 0 && this.maxRow > 0);
      assert(this.maxCol === this.array.length && this.maxRow === this.array[0].length);
    }
    get(r, c) {
      assert(0 <= r && r < this.maxRow && 0 <= c && c < this.maxCol);
      return this.array[c][r];
    }
    set(r, c, v) {
      assert(0 <= r && r < this.maxRow && 0 <= c && c < this.maxCol);
      this.array[c][r] = v;
    }
    toColumns() {
      return this.array;
    }
    static fromColumns(columns) {
      assert(columns.length > 0 && columns[0].length > 0);
      return new ColumnMatrix64(columns);
    }
    static createRandom(numRows, numCols) {
      assert(numRows > 0 && numCols > 0);
      const matrix = new ColumnMatrix64(numRows, numCols);
      return this._createRandom(matrix);
    }
    mm(matrix) {
      assert(matrix instanceof ColumnMatrix64);
      return ColumnMatrix64.fromColumns(this._mm2(matrix));
    }
  }
  exports.ColumnMatrix64 = ColumnMatrix64;
});
