//
//  Read numpy array into a typed array.
//  Used the numpy spec https://numpy.org/doc/stable/reference/generated/numpy.lib.format.html .
//  Supports numpy v1 and v2; v3 is for a utf8 header and is not supported yet except for the 0 -> 0xFF subset, of course.
//  Grabbed the hdrStr replace strings from https://github.com/propelml/tfjs-npy/blob/master/npy.ts
//  pfr
//  ChrisM
//  Nov 1, 2021
//
const fs = require('fs');

exports.npyToShapedArray = function _npyToShapedArray(fileName, addSuffix = true) {
  const b8 = exports.readNumPy(fileName, addSuffix);
  const shapedArray = exports.npyBytesToShapedArray(b8);
  return shapedArray;
}
exports.readNumPy = function _readNumPy(fileName, addSuffix = true) {
  let fd;
  try {
    if (addSuffix) fileName += '.npy';
    const state = fs.statSync(fileName);
    const b8 = new Uint8Array(state.size);
    fd = fs.openSync(fileName, 'r')
    fileLength = fs.readSync(fd, b8, 0, b8.byteLength, 0);
    if (fileLength != b8.byteLength)
      throw new Error(`Expected to read ${b8.byteLength} from file ${fileName}, but read ${fileLength} instead.`);
    return b8;
  } catch (e) {
    console.log(e);
    throw e;
  } finally {
    if (fd) fs.closeSync(fd);
  }
}
exports.transmitTypedArray = function _transmitTypedArray(fileName, shapedArray) {
  exports.checkShapedArray(shapedArray, fileName);
  const b64 = exports.typedArrayToBase64(shapedArray.typedArray);
  fs.writeFileSync(fileName, b64, 'binary');
}
exports.typedArrayToBase64 = function _typedArrayToBase64(typedArray) {
  const buffer = Buffer.from(typedArray.buffer);
  const b64 = buffer.toString('base64');
  return b64;
}
exports.typedArrayToEncodedBase64 = function _typedArrayToEncodedBase64(typedArray) {
  const b64 = exports.typedArrayToBase64(typedArray);
  return encodeURIComponent(b64);
}
exports.base64ToTypeArray_Node = function _base64ToTypeArray_Node(b64) {
  const buffer = Buffer.from(b64, 'base64');
  // 'latin1' == ISO-9959-1 only supports U+0000 => U+00FF, which is just what we need for binary data.
  return buffer.toString('latin1');
}
exports.checkShapedArray = function _checkShapedArray(shapedArray, name) {
  const { typedArray, shape } = shapedArray;
  console.log(`${name}: ${typedArray.constructor.name}, ${typedArray.length}, ${shape}, ${typedArray.BYTES_PER_ELEMENT}, ${typedArray.byteOffset}`);
}
function uint8ToString(b8, offset, length) {
  return Buffer.from(b8.buffer, offset, length).toString();
}
function b64ToUint6 (nChr) {
  return nChr > 64 && nChr < 91  ? nChr - 65
       : nChr > 96 && nChr < 123 ? nChr - 71
       : nChr > 47 && nChr < 58  ? nChr + 4
       : nChr === 43 ? 62
       : nChr === 47 ? 63 : 0;
}
exports.base64ToUint8Array = function _base64ToUint8Array(b64) {
  if (!b64) throw new Error('base64ToUint8Array: base64 input string is undefined.');
  const sB64Enc = b64.replace(/[^A-Za-z0-9+/]/g, ""), nInLen = sB64Enc.length; b64 = null;
  const nOutLen = (nInLen * 3 + 1) >> 2, taBytes = new Uint8Array(nOutLen);
  for (let nMod3, nMod4, nUint24 = 0, nOutIdx = 0, nInIdx = 0; nInIdx < nInLen; nInIdx++) {
    nMod4 = nInIdx & 3;
    nUint24 |= b64ToUint6(sB64Enc.charCodeAt(nInIdx)) << (6 * (3 - nMod4));
    if (nMod4 === 3 || nInLen - nInIdx === 1) {
      for (nMod3 = 0; nMod3 < 3 && nOutIdx < nOutLen; nMod3++, nOutIdx++)
        taBytes[nOutIdx] = (nUint24 >>> ((16 >>> nMod3) & 24)) & 255;
      nUint24 = 0;
    }
  }
  return taBytes;
}

function uint6ToB64(nUint6) {
  return nUint6 < 26 ? nUint6 + 65
       : nUint6 < 52 ? nUint6 + 71
       : nUint6 < 62 ? nUint6 - 4
       : nUint6 === 62 ? 43
       : nUint6 === 63 ? 47 : 65;
}
exports.binaryToBase64 = function binaryToBase64(taBytes) {
  var nMod3 = 2, sB64Enc = "";
  for (var nLen = taBytes.length, nUint24 = 0, nIdx = 0; nIdx < nLen; nIdx++) {
    nMod3 = nIdx % 3;
    if (nIdx > 0 && (nIdx * 4 / 3) % 76 === 0) { sB64Enc += "\r\n"; }
    nUint24 |= taBytes[nIdx] << (16 >>> nMod3 & 24);
    if (nMod3 === 2 || taBytes.length - nIdx === 1) {
      sB64Enc += String.fromCodePoint(uint6ToB64(nUint24 >>> 18 & 63), uint6ToB64(nUint24 >>> 12 & 63), 
                                      uint6ToB64(nUint24 >>> 6 & 63), uint6ToB64(nUint24 & 63));
      nUint24 = 0;
    }
  }
  return sB64Enc.substring(0, sB64Enc.length - 2 + nMod3) + (nMod3 === 2 ? '' : nMod3 === 1 ? '=' : '==');
}
exports.base64ToFloat32Array = function _base64ToFloat32Array(b64) {
  const b8 = exports.base64ToUint8Array(b64);
  b64 = null;
  return new Float32Array(b8.buffer);
}
exports.npyBase64ToTypedArray = function _npyBase64ToTypedArray(b64) {
  const b8 = exports.base64ToUint8Array(b64);
  b64 = null;
  return exports.npyBytesToShapedArray(b8);
}
exports.createTypedArray = function _createTypedArray(b8, descr) {
  switch (+descr[2] | 0) {
    case 1: return (descr[1] === 'u') ? new Uint8Array(b8.buffer) : new Int8Array(b8.buffer);
    case 2: return (descr[1] === 'u') ? new Uint16Array(b8.buffer) : new Int16Array(b8.buffer);
    case 4: return (descr[1] === 'f') ? new Float32Array(b8.buffer) : (descr[1] === 'u') ? new Uint32Array(b8.buffer) : new Int32Array(b8.buffer);
    case 8: return (descr[1] === 'f') ? new Float64Array(b8.buffer) : (descr[1] === 'u') ? new BigUint64Array(b8.buffer) : new BigInt64Array(b8.buffer);
    default: throw new Error(`Unknown numpy descriptor ${descr}`);
  }
}
exports.npyBytesToShapedArray = function _npyBytesToShapedArray(b8) {
  // Verify NumPy magic bytes.
  const npHdr = [ 0x93, 0x4e, 0x55, 0x4d, 0x50, 0x59, 0x1, 0x0 ];
  let offset = npHdr.length;
  for (let k = 0; k < offset; k++) {
    if (b8[k] !== npHdr[k])
      throw new Error(`The numpy header byte # ${k} should be ${npHdr[k]}, not ${b8[k]}`);
  }

  // Compute header length.
  const shortHdr = b8[offset + 2] === 0x7b; // Check whether hdr can fit in 64K .
  const dv = new DataView(b8.buffer, offset, 4);
  let hdrLen;
  if (shortHdr) {
    hdrLen = dv.getInt16(0, true);
    offset += 2;
  } else {
    hdrLen = dv.getInt32(0, true);
    offset += 4;
  }

  // Parse dictionary portion of header as JSON.
  const hdrStr = uint8ToString(b8, offset, hdrLen);
  const hdrJson = hdrStr.replace("True", "true")
                        .replace("False", "false")
                        .replace(/'/g, `"`)
                        .replace(/,\s*}/, " }")
                        .replace(/,?\)/, "]")
                        .replace("(", "[");
  const { descr, fortran_order, shape } = JSON.parse(hdrJson);

  // Check for unsupported fortran_order.
  if (fortran_order)
    throw Error('numPyToTypedArray does not support fortran_order yet. Implement it now!');

  // Compute # of elements.
  let totalSize = shape.length > 0 ? shape[0] : 1;
  for (let k = 1; k < shape.length; k++)
    totalSize *= shape[k];

  // Create typedArray.
  offset += hdrLen;

  const truncatedTypedArray = b8.slice(offset);
  b8_byteLength = b8.byteLength;
  b8 = null;
  const typedArray = exports.createTypedArray(truncatedTypedArray, descr);

  // Sanity check.
  totalSize *= typedArray.BYTES_PER_ELEMENT;
  if (b8_byteLength !== totalSize + offset || totalSize != typedArray.byteLength)
    throw new Error(`Inconsistent sizes in parsing numpy array ${b8_byteLength}, ${totalSize}, ${typedArray.byteLength}, ${offset}`);
  b8 = null;

  return { typedArray: typedArray, dtype: descr, shape: shape };
}
